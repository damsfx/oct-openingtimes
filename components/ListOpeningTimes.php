<?php
namespace Zwiebl\Openingtimes\Components;

use Spatie\OpeningHours\OpeningHours;
use Carbon\Carbon;
use DateTime;
use Zwiebl\Openingtimes\Models\Settings;

class ListOpeningTimes extends \Cms\Classes\ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Show Opening Times',
            'description' => 'Displays opening times'
        ];
    }

    public function onRun() {
        $this->page["nowOpen"] = $this->page->nowOpen = $this->isNowOpen();
        $this->getOpenAtDays();
        $this->nextOpen();

    }

    public function defineProperties()
    {
        return [
            'shortDayNames' => [
                'title'             => 'Short Day Names',
                'description'       => 'Use short day names',
                'default'           => false,
                'type'              => 'checkbox',
                'validationMessage' => 'Short Day Names field is wrong'
            ]
        ];
    }

    private function nextOpen() {
        $ot = $this->getOpeningTimes();

        if($ot){
            //setlocale(LC_TIME, 'de_DE.utf8')
            $nextOpen = Carbon::createFromTimestamp($ot->nextOpen(new DateTime())->getTimestamp());

            $nextOpenLFormat = '';
            $nextOpen->setLocale( config('app.locale') );
            setlocale(LC_TIME, config('app.locale') );
            $nextOpenLFormat = $nextOpen->formatLocalized('%A, %d. %B %Y - %H:%M');

            $this->page["otNextOpen"] = $this->page->otNextOpen = $nextOpenLFormat;


            $nextOpenTimeOnly = $nextOpen->toTimeString();
            $this->page["otNextOpenTimeOnly"] = $this->page->otNextOpenTimeOnly = $nextOpenTimeOnly;

            //dd($ot->nextOpen(new DateTime()));
            if($nextOpen->isToday()) {

                $this->page["otNextOpen"] = $this->page->otNextOpen = "Heute ab " . substr($nextOpenTimeOnly, 0,5);
            }

        }
        else {
            return false;
        }

    }

    private function getOpenAtDays() {
        $ot = $this->getOpeningTimes();
        if($ot) {
            $this->page["otMonday"] = $this->page->otMonday = $ot->forDay('monday');
            //dd($this->page->otMonday);
            $this->page["otTuesday"] = $this->page->otTuesday = $ot->forDay('tuesday');
            $this->page["otWednesday"] = $this->page->otWednesday = $ot->forDay('wednesday');
            $this->page["otThursday"] = $this->page->otThursday = $ot->forDay('thursday');
            $this->page["otFriday"] = $this->page->otFriday = $ot->forDay('friday');
            $this->page["otSaturday"] = $this->page->otSaturday = $ot->forDay('saturday');
            $this->page["otSunday"] = $this->page->otSunday = $ot->forDay('sunday');
        }
    }

    public function isNowOpen() {
        $dt = Carbon::now();
        $dt = new DateTime();
        $ot = $this->getOpeningTimes();
        if($ot)
            return $now = $ot->isOpenAt($dt);
    }

    private function getOpeningTimes() {

        // Create OpeningTimes from Backend Settings / not really fertig.. haha
        $oTimes = Settings::instance()->oTimes;

        //dd($settings);
        $nOTimes = array();
        if(!empty($oTimes)) {
            foreach($oTimes as $row) {

                //dd($row);


                foreach ($row['days'] as $day) {


                    //dd($row);


                    // if the days of this row should be closed
                    if($row['closed']) {
                        $nOTimes[$day] = [];
                    }
                    else {
                        // if not, there must be any opening times set
                        $arrTimes = [];
                        if (array_key_exists('timerepeater', $row)) {
                            foreach ($row['timerepeater'] as $time) {
                                $tf = new Carbon($time["time_from"]);
                                $tt = new Carbon($time["time_till"]);
                                $time = $tf->format('H:i') . "-" . $tt->format('H:i');
                                $arrTimes[] = $time;
                            }
                        }

                        $nOTimes[$day] = $arrTimes;
                    }

                }

            }
        }
        else {
            return false;
        }


        // Add Exceptions
        if($exSettings = Settings::instance()->exceptions) {
            $exceptions = [];
            foreach($exSettings as $exDay) {
                //dd($exDay);
                $exDate = new Carbon($exDay['date']);

                if($exDay['closed']) {
                    $exceptions[$exDate->toDateString()] = [];
                }
                else { // not closed
                    $arrTimes = [];
                    if(array_key_exists('timerepeater', $exDay)) {
                        foreach ($exDay['timerepeater'] as $time) {
                            $tf = new Carbon($time["time_from"]);
                            $tt = new Carbon($time["time_till"]);
                            $time = $tf->format('H:i') . "-" . $tt->format('H:i');
                            $arrTimes[] = $time;
                        }
                    }
                    $exceptions[$exDate->toDateString()] = $arrTimes;
                }

            }
            $nOTimes['exceptions'] = $exceptions;
        }
        //dd($nOTimes);
        //return OpeningHours::create($nOTimes);

        // Static Opening Times Array // when testing, use this. $nOTimes sollte ähnlichen (generierten Inhalt haben)
        $arrstaticOpeningTimes = [
            'monday' => ['08:15-08:16'],
            'tuesday' => ['08:15-08:16'],
            'wednesday' => ['08:15-08:16'],
            'thursday' => ['08:15-08:16'],
            'friday' => ['08:15-08:16'],
            'saturday' => ['08:15-08:16'],
            'sunday' => ['08:15-08:16'],
        ];

        if((!array_key_exists('monday', $nOTimes)) || (!array_key_exists('tuesday', $nOTimes)) || (!array_key_exists('wednesday', $nOTimes)) || (!array_key_exists('thursday', $nOTimes)) || (!array_key_exists('friday', $nOTimes)) || (!array_key_exists('saturday', $nOTimes)) || (!array_key_exists('sunday', $nOTimes))) {
            // One day is missing
            $this->page['dayMissing'] = true;
            return OpeningHours::create($arrstaticOpeningTimes);
        }
        else {
            return OpeningHours::create($nOTimes);
        }

    }

    public function getBackendOTimesRepeater()
    {
        return Settings::instance();
    }

    public function getBackendSettings()
    {
        return Settings::instance();
    }

}